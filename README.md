# Support Driven Salary Survey

Each year, Support Driven collaborates with the community and companies to survey customer support professionals about what they earn relative to their experience, company size, location, gender and other factors. We then analyze that survey data and publish a study and salary calculator.

Over the years, the customer support salary study has become an indispensable data point in a broader and evolving discussion about the value of customer service work — a critical discussion to have, when companies are increasingly competing on customer experience, and support professionals have greater choices among employers who value their skills and compensate accordingly.

We continue to conduct and publish this study so that managers, employees and job seekers can have more informed conversations about compensation and professional development.

## About this project
This is a work in progress. More info to come, stay tuned!

### Project Goal
One of our goals is to Open Source this process, and provide greater transparency and opportunity for community involvement.

### Timeline
The goal is to release the final survey by end of April.

### Things we need to do ...
* [x]  Create initial structure for the master branch (2019 survey)
* [x]  Create branches for previous years
* [ ]  Create project structure for Issues/Epics etc 
* [ ]  Identify contributors to the project and schedule an initial conversation
* [ ]  ... more stuff, surely ....
* [ ]  Announce the project to the community 
* [ ]  ... even more stuff ...


